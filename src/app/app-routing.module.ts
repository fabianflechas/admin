import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminGuard } from './core/guards/admin.guard';
import { SigninAdminComponent } from './core/components/signin-admin/signin.component';
import { AuthenticationGuard } from './core/guards/authentication.guard';

const routes: Routes = [
  {
    path: '',
    loadChildren: 'app/admin/admin.module#AdminModule',
    canActivate: [AuthenticationGuard, AdminGuard]
  },
  {
    path: 'signin-admin',
    component: SigninAdminComponent
  },
  {
    path: '**',
    pathMatch: 'full',
    redirectTo: '/regalos'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
