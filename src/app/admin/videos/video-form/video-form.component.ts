import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { DialogService } from '../../../core/components/dialog/dialog.service';
import { IVideo } from '../../../core/model/IVideo';
import { States } from '../../../core/model/states';
import { VideoService } from '../video.service';
import { take } from 'rxjs/operators';
import { BaseItemForm } from '../../items/base-item-form';
import { VideoTagService } from '../../tags/video-tag.service';

declare var $;

@Component({
  selector: 'app-video-form',
  templateUrl: './video-form.component.html',
  styleUrls: ['./video-form.component.scss', '../../items/base-item-form.scss']
})
export class VideoFormComponent extends BaseItemForm<IVideo> implements OnInit {

  video: IVideo;
  create: boolean = true;

  constructor(
    private route: ActivatedRoute,
    private videoService: VideoService,
    private dialogService: DialogService,
    private router: Router,
    private tagService: VideoTagService
  ) {
    super(videoService)
  }

  ngOnInit() {

    this.tagService.getAll().subscribe(
      tags => {
        this.tags = tags.map(tag => {
          delete tag.items;
          return tag;
        });
        this.getVideo();
      }
    );

  }

  getVideo() {
    // procesar el id pasado por parametro en la ruta
    this.route.paramMap
      .subscribe((params: ParamMap) => {
        // si hay un parametro id, es porque estamos editando un video
        if (params.get('id') != null) {
          this.create = false;
          //  (+) before `params.get()` turns the string into a number
          console.log('get video: ', params.get('id'));
          this.videoService.get(params.get('id'))
            .pipe(
              take(1)
            )
            .subscribe(
              video => {
                this.video = video;
                this.video.oldTitle = this.video.title;
                // mapea del formato guardado en la base de datos a un formato que se puede usar en el form
                this.selectedTags = this.mapToFormTags(video);
                this.deletedTagsKeys = [];
              },
              error => {
                this.dialogService.openDialog('error al cargar el video: ' + error.message, '', 'warning');
              }
            )
        } else { // si no hay un parametro es porque estamos creando el alumno
          this.create = true;
          this.selectedTags, this.deletedTagsKeys = [];
          this.video = {
            date: Date.now(),
            description: '',
            state: States.PUBLIC,
            title: '',
            link: '',
          };

        }
      });
  }

  submit() {
    if (this.titleInvalid) return;
    this.video.tags = this.mapToDatabseTags();
    if (this.create) {
      this.videoService.save(this.video)
        .then(
          succss => {
            this.dialogService.openDialog('el video se guardo con exito', '', 'success');
            this.router.navigate(['/videos']);
          }
        )
        .catch(
          error => {
            this.dialogService.openDialog('error al cargar el video: ' + error, '', 'warning');
          }
        )
    } else {
      this.videoService.update(this.video, this.deletedTagsKeys)
        .then(
          succss => {
            this.dialogService.openDialog('el video se guardo con exito', '', 'success');
            this.router.navigate(['/videos']);
          }
        )
        .catch(
          error => {
            this.dialogService.openDialog('error al cargar el video: ' + error, '', 'warning');
          }
        )
    }
  }

  removeVideo() {
    this.dialogService.openAskDialog('Esta seguro de eliminar este video?', '')
      .subscribe(
        confirm => {
          if (confirm) {
            this.videoService.remove(this.video);
            this.router.navigate(['/videos']);
          }
        }
      );
  }

}
