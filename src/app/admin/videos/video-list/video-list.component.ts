import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { States } from '../../../core/model/states';
import { IVideo } from '../../../core/model/IVideo';
import { VideoService } from '../video.service';

@Component({
  selector: 'app-gift-list',
  templateUrl: './video-list.component.html',
  styleUrls: ['./video-list.component.scss']
})
export class VideoListComponent implements OnInit {

  videos: Observable<IVideo[]>;

  constructor(
    private giftService: VideoService,
    private router: Router
  ) { }

  ngOnInit() {
    this.videos = this.giftService.getAll();
  }

  edit(video: IVideo) {
    this.router.navigate(['/formulario-video', { id: video.$key }]);
  }

  get States() {
    return States;
  }

  getStateName(state): String {
    // the state can be a string or a number .... horrible :/
    switch (+state) {
      case States.PUBLIC:
        return 'Publico';
      case States.PRIVATE:
        return 'Privado';
      case States.AUTH:
        return 'Autenticado';
      default:
        return 'Publico';
    }
  }

}
