import { Injectable } from '@angular/core';
import { ItemService } from '../items/item.service';
import { AngularFireDatabase } from 'angularfire2/database';
import { IVideo } from '../../core/model/IVideo';


@Injectable()
export class VideoService extends ItemService<IVideo> {

  baseUrl: string = '/Videos';
  baseTagUrl: string = '/TagsVideo';
  baseTitleUrl: string = '/Titles/videos';

  constructor(
    _db: AngularFireDatabase
  ) {
    super(_db)
  }

}
