//  modules
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { FormsModule } from '@angular/forms';
//  reactiveFormsModule are in the core module

//  routing module
import { AdminRoutingModule } from './admin-routing.module';

//  services
import { AdminUsersService } from './admin-users/admin-users.service';

//  components
import { AdminComponent } from './admin.component';
import { ArticleFormComponent } from './articles/article-form/article-form.component';
import { ArticleListComponent } from './articles/article-list/article-list.component';
import { GiftFormComponent } from './gifts/gift-form/gift-form.component';
import { GiftListComponent } from './gifts/gift-list/gift-list.component';
import { TinyMCEEditorComponent } from './helper-components/tiny-mce-editor/tiny-mce-editor.component';
import { TinyUploadService } from './helper-components/tiny-mce-editor/upload.service';
import { ArticleService } from './articles/article.service';
import { AdminAssociatedFormComponent } from './admin-users/admin-associated-form/admin-associated-form.component';
import { AdminFormComponent } from './admin-users/admin-form/admin-form.component';
import { AdminAdministrationComponent } from './admin-users/admin-administration/admin-administration.component';
import { GiftsService } from './gifts/gifts.service';
import { TagListComponent } from './tags/tag-list/tag-list.component';
import { TagFormComponent } from './tags/tag-form/tag-form.component';
import { VideoListComponent } from './videos/video-list/video-list.component';
import { VideoFormComponent } from './videos/video-form/video-form.component';
import { VideoService } from './videos/video.service';
import { CustomMaterialModule } from '../custom-material/custom-material.module';
import { VideoTagService } from './tags/video-tag.service';
import { GiftTagService } from './tags/gift-tag.service';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CoreModule,
    AdminRoutingModule,
    CustomMaterialModule
  ],
  declarations: [
    AdminComponent,
    ArticleFormComponent,
    ArticleListComponent,
    GiftFormComponent,
    GiftListComponent,
    TinyMCEEditorComponent,
    AdminAdministrationComponent,
    AdminFormComponent,
    AdminAssociatedFormComponent,
    TagListComponent,
    TagFormComponent,
    VideoListComponent,
    VideoFormComponent
  ],
  providers: [
    TinyUploadService,
    ArticleService,
    AdminUsersService,
    GiftsService,
    VideoService,
    VideoTagService,
    GiftTagService
  ]
})
export class AdminModule { }

