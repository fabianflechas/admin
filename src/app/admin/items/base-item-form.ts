import { ITag } from '../../core/model/ITag';
import { IItem } from '../../core/model/IItem';
import { IGroup } from '../../core/model/IGroup';
import { ItemService } from './item.service';
import { States } from '../../core/model/states';

export interface IFormTag {
    tag: ITag,
    groups: IGroup[],
    created: boolean
}

export abstract class BaseItemForm<T> {

    tags: ITag[] = [];
    selectedTags: IFormTag[] = [];
    deletedTagsKeys: string[] = [];
    titleInvalid = false;
    states;

    dropdownSettings = {
        singleSelection: false,
        text: 'Elige los grupos',
        enableCheckAll: false,
        enableSearchFilter: true,
        labelKey: 'name',
        primaryKey: '$key',
        noDataLabel: 'No tienes grupos o se estan cargando desde el servidor'
    };

    constructor(
        private itemService: ItemService<T>
    ) {
        this.states = States;
    }

    mapToFormTags(gift: IItem): IFormTag[] {

        let formTags = []
        for (var key in gift.tags) {
            if (gift.tags.hasOwnProperty(key)) {
                // se extraen los datos del tag
                let mapTag = this.tags.find(t => t.$key == key);
                let selectedTag = {
                    tag: mapTag,
                    created: false,
                    groups: mapTag.groups.filter(g => gift.tags[key].groups.indexOf(g.$key) > -1)
                };
                formTags.push(selectedTag);
            }
        }
        return formTags;
    }

    mapToDatabseTags() {
        let databaseTags = {};
        this.selectedTags.forEach((tag: IFormTag) => {
            databaseTags[tag.tag.$key] = {};
            databaseTags[tag.tag.$key].groups = tag.groups.map(group => group.$key);
        });
        return databaseTags;
    }

    removeTag(index) {
        //si el tag no fue creado en esta carga se anota a la lista de eliminados
        if (this.selectedTags[index].created == false) {
            this.deletedTagsKeys.push(this.selectedTags[index].tag.$key);
        }
        this.selectedTags.splice(index, 1)
    }

    addTag() {
        this.selectedTags.push({
            tag: null,
            created: true,
            groups: []
        });
    }

    resetGroups(index) {
        this.selectedTags[index].groups = [];
    }

    tagAlreadySelected(tagKey) {
        return this.selectedTags
            .find(selectedTag => {
                return selectedTag.tag ? selectedTag.tag.$key == tagKey : false
            }) !== undefined
    }

    get notAvailableTags() {
        return this.tags
            .filter(tag => this.selectedTags
                .find(selectedTag => {
                    return selectedTag ? selectedTag.tag.$key == tag.$key : false
                })
                == undefined).length == 0;
    }

    verifyTitle(item: IItem) {
        console.log('checking title...');
        // tslint:disable-next-line:curly
        if (item.title === item.oldTitle) return;
        this.itemService.checkTitle(item.title)
            .subscribe(
                success => {
                    console.log('title...', success);
                    this.titleInvalid = !success;
                },
                error => {
                    console.log('error al verificar titutlo');
                }
            );
    }

}