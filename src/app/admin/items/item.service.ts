import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { IItem } from '../../core/model/IItem';

export abstract class ItemService<T> {

    abstract baseUrl: string;
    abstract baseTagUrl: string;
    abstract baseTitleUrl: string;

    constructor(
        private db: AngularFireDatabase
    ) { }


    public getAll(): Observable<T[]> {
        return this.db.list(`${this.baseUrl}`);
    }

    public get(key): Observable<T> {
        return this.db.object(`${this.baseUrl}/${key}`);
    }

    public save(item: IItem) {
        delete item.$key;
        return new Promise((resolve, reject) => {
            this.db.list(`${this.baseUrl}`).push(item)
                .then(
                    result => {
                        item.$key = result.key;
                        let titulo = item.title.replace(/ /g, '-');
                        console.log('result: ', result.key);
                        this.db.object(`${this.baseTitleUrl}/${titulo.toLowerCase()}`).set(result.key)
                            .then(
                                response => {
                                    this.updateItemInTags(item);
                                    resolve();
                                }
                            )
                    }
                )
                .catch(
                    error => {
                        reject(error);
                    }
                )
        });
    }

    public update(item: IItem, deletedTagsKeys: string[]) {
        let key = item.$key;
        if (key == undefined) return Promise.reject('Error al obtener la key del objeto que se quiere guardar');
        delete item.$key;
        return new Promise((resolve, reject) => {
            this.db.object(`${this.baseUrl}/${key}`).update(item)
                .then(
                    result => {
                        let title = item.title.toLowerCase().replace(/ /g, '-');
                        let oldTitle = item.oldTitle.toLowerCase().replace(/ /g, '-');
                        this.db.object(`${this.baseTitleUrl}/${oldTitle}`).remove();
                        this.db.object(`${this.baseTitleUrl}/${title}`).set(key)
                            .then(
                                response => {
                                    item.$key = key;
                                    this.updateItemInTags(item);
                                    this.removeItemFromTags(item, deletedTagsKeys);
                                    resolve();
                                }
                            )
                    }
                )
                .catch(
                    error => {
                        reject(error);
                    }
                )
        });
    }

    public remove(item: IItem) {
        let key = item.$key;
        delete item.$key;
        Object.keys(item.tags).forEach(tagKey => {
            this.removeItemFromTag(key, tagKey);
        });
        return this.db.object(`${this.baseUrl}/${key}`).remove();
    }

    public checkTitle(title: string) {
        title = title.toLowerCase();
        return this.db.object(`${this.baseTitleUrl}/${title.replace(/ /g, '-')}`)
            .pipe(
                map(title => {
                    return !title.$value
                })
            )
    }

    private updateItemInTags(item: IItem) {
        for (var key in item.tags) {
            if (item.tags.hasOwnProperty(key)) {
                this.addItemToTag(item, key)
            }
        }
    }

    private removeItemFromTags(item: IItem, deletedTagKeys: string[]) {
        deletedTagKeys.forEach(tagKey => {
            this.removeItemFromTag(item.$key, tagKey)
        });
    }

    private addItemToTag(item: IItem, tagKey) {

        this.db.object(`${this.baseTagUrl}/${tagKey}/items/${item.$key}/groups`).set(item.tags[tagKey].groups);

    }

    private removeItemFromTag(itemKey, tagKey) {

        this.db.object(`${this.baseTagUrl}/${tagKey}/items/${itemKey}`).remove();

    }

}
