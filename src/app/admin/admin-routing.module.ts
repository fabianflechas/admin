import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { ArticleListComponent } from './articles/article-list/article-list.component';
import { ArticleFormComponent } from './articles/article-form/article-form.component';
import { GiftListComponent } from './gifts/gift-list/gift-list.component';
import { GiftFormComponent } from './gifts/gift-form/gift-form.component';
import { AdminAdministrationComponent } from './admin-users/admin-administration/admin-administration.component';
import { AdminFormComponent } from './admin-users/admin-form/admin-form.component';
import { AdminAssociatedFormComponent } from './admin-users/admin-associated-form/admin-associated-form.component';
import { TagListComponent } from './tags/tag-list/tag-list.component';
import { TagFormComponent } from './tags/tag-form/tag-form.component';
import { VideoListComponent } from './videos/video-list/video-list.component';
import { VideoFormComponent } from './videos/video-form/video-form.component';


const routes: Routes = [
  {
    path: '',
    component: AdminComponent,
    children: [
      {
        path: 'articulos',
        component: ArticleListComponent
      },
      {
        path: 'formulario-articulo',
        component: ArticleFormComponent
      },
      {
        path: 'regalos',
        component: GiftListComponent
      },
      {
        path: 'video-tags',
        component: TagListComponent,
        data: { tagType: "video" }
      },
      {
        path: 'gift-tags',
        component: TagListComponent,
        data: { tagType: "gift" }
      },
      {
        path: 'formulario-video-tag',
        component: TagFormComponent,
        data: { tagType: 'video' }
      },
      {
        path: 'formulario-gift-tag',
        component: TagFormComponent,
        data: { tagType: 'gift' }
      },
      {
        path: 'videos',
        component: VideoListComponent
      },
      {
        path: 'formulario-video',
        component: VideoFormComponent
      },
      {
        path: 'formulario-regalo',
        component: GiftFormComponent
      },
      {
        path: 'administradores',
        component: AdminAdministrationComponent
      },
      {
        path: 'crear-administrador',
        component: AdminFormComponent
      },
      {
        path: 'crear-administrador-suscripto',
        component: AdminAssociatedFormComponent
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
