export class Upload {

  file:File;
  url:string;
  progress:number;

  constructor(file:File) {
    this.file = file;
  }

}
