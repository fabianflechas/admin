import { AfterViewInit, Component, EventEmitter, Input, OnDestroy, Output } from '@angular/core';
import { Upload } from './upload';
import { TinyUploadService } from './upload.service';

declare let tinymce: any;

@Component({
  selector: 'app-tiny-mce-editor',
  templateUrl: './tiny-mce-editor.component.html',
  styleUrls: ['./tiny-mce-editor.component.scss']
})
export class TinyMCEEditorComponent implements AfterViewInit, OnDestroy {
  [name: string]: any;

  @Output() onEditorKeyup = new EventEmitter<any>();
  @Input() set content($event) {
    if (this.editor) this.editor.setContent($event);
  };
  editor;

  constructor(
    private tinyUploadService: TinyUploadService
  ) { }

  ngAfterViewInit() {
    tinymce.init({
      selector: '#editorTinyMce',
      plugins: [
        'paste', 'table', 'textcolor',
        'print', 'preview', 'lists',
        'contextmenu', 'fullscreen', 'image',
        'media'
      ],
      skin_url: '../../../../assets/skins/lightgray',
      menubar: false,
      toolbar: [
        `undo redo | fontselect fontsizeselect | underline bold italic| forecolor backcolor
         | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent 
         | image media | preview print fullscreen`
      ],
      fontsize_formats: '8pt 10pt 12pt 14pt 16pt 18pt 24pt 36pt 48pt 64pt',
      plugin_preview_height: 500,
      plugin_preview_width: 800,
      image_title: true,
      automatic_uploads: true,
      file_picker_types: 'image',
      file_picker_callback: function (cb, value, meta) {
        let input = document.createElement('input');
        input.setAttribute('type', 'file');
        input.setAttribute('accept', 'image/*');

        //  Note: In modern browsers input[type='file'] is functional without
        //  even adding it to the DOM, but that might not be the case in some older
        //  or quirky browsers like IE, so you might want to add it to the DOM
        //  just in case, and visually hide it. And do not forget do remove it
        //  once you do not need it anymore.

        input.onchange = function () {
          let file = input.files[0];

          let reader = new FileReader();
          reader.onload = function () {
            //  Note: Now we need to register the blob in TinyMCEs image blob
            //  registry. In the next release this part hopefully won't be
            //  necessary, as we are looking to handle it internally.
            let id = 'blobid' + (new Date()).getTime();
            let blobCache = tinymce.activeEditor.editorUpload.blobCache;
            let base64 = (<string>reader.result).split(',')[1];
            let blobInfo = blobCache.create(id, file, base64);
            blobCache.add(blobInfo);

            //  call the callback and populate the Title field with the file name
            cb(blobInfo.blobUri(), { title: file.name });
          };
          reader.readAsDataURL(file);
        };

        input.click();
      },
      images_upload_handler: (blobInfo, success, failure) => {
        console.log('subiendo imagen', blobInfo);
        let upload = new Upload(blobInfo.blob());
        this.tinyUploadService.pushUpload(upload).subscribe(
          change => {
            // se cargo un poquito
          },
          (error: any) => {
            failure('Error: ' + error.message);
          },
          () => {
            console.log('exito al subir imagen, url: ', upload.url);
            success(upload.url);
          }
        );
      },
      setup: editor => {
        this.editor = editor;
        editor.on('change keyup', () => {
          const content = editor.getContent();
          this.onEditorKeyup.emit(content);
        });
      },
    });
  }

  ngOnDestroy() {
    tinymce.remove(this.editor);
  }

}
