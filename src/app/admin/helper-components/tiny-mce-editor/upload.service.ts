import {Upload} from './upload';
import {Injectable} from '@angular/core';
import {AngularFireDatabase} from 'angularfire2/database';

import * as firebase from 'firebase/app'; 
import { Subject } from 'rxjs';

@Injectable()
export class TinyUploadService {

  private basePath:string = '/Article';

  constructor(
    private db: AngularFireDatabase
  ) {}

  pushUpload(upload: Upload) {

    let result : Subject<Upload> = new Subject();

    let storageRef = firebase.storage().ref();
    let uploadTask = storageRef.child(`${this.basePath}/${Date.now()}`).put(upload.file);

    uploadTask.on(firebase.storage.TaskEvent.STATE_CHANGED,
      (snapshot: firebase.storage.UploadTaskSnapshot) =>  {
        //  upload in progress
        upload.progress = (snapshot.bytesTransferred / snapshot.totalBytes) * 100;
        result.next(upload);
      },
      (error) => {
        //  upload failed
        console.log(error);
        result.error(error)
      },
      () => {
        //  upload success
        upload.url = uploadTask.snapshot.downloadURL;
        result.next(upload);
        result.complete();
      }
    );

    return result;
  }

  deleteUpload(upload: Upload) {
  }

  //  Deletes the file details from the realtime db
  private deleteFileData(key: string) {
    return this.db.list(`${this.basePath}/`).remove(key);
  }

  //  Firebase files must have unique names in their respective storage dir
  //  So the name serves as a unique key
  private deleteFileStorage(name:string) {
    let storageRef = firebase.storage().ref();
    storageRef.child(`${this.basePath}/${name}`).delete()
  }

}
