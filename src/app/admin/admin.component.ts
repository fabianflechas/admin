import { Component, OnInit } from '@angular/core';
import { AuthenticationService } from '../core/authentication/authentication.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  nombreUsuario = 'admin';

  constructor(
    private authService: AuthenticationService,
    private router: Router
  ) { }

  ngOnInit() {
    this.authService.getAdminUser()
      .subscribe(
        user => {
          this.nombreUsuario = user.displayName;
        }
      )
  }

  logOut() {
    this.authService.logout()
      .then(
        res => {
          this.router.navigate(['/signin-admin'])
        }
      )
  }

}
