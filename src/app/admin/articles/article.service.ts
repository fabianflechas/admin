import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { Article } from '../../core/model/article';
import { map } from 'rxjs/operators';


@Injectable()
export class ArticleService {

  baseUrl = '/Articles';
  baseTitleUrl = '/Titles/articles';

  constructor(
    private db: AngularFireDatabase
  ) { }

  //  Get all the artivles
  getAll(): Observable<Article[]> {
    return this.db.list(`${this.baseUrl}`)
      .pipe(
        map(Article.fromJsonArray)
      )
  }

  get(key): Observable<Article> {
    return this.db.object(`${this.baseUrl}/${key}`)
      .pipe(
        map(articulo => Article.fromJson(articulo))
      )
  }

  save(articulo: Article) {
    delete articulo.$key;
    return new Promise((resolve, reject) => {
      this.db.list(`${this.baseUrl}`).push(articulo)
        .then(
          result => {
            if (!result.key) { reject('ocurrio un problema, intenta de nuevo') }
            const titulo = articulo.title.replace(/ /g, '-');
            console.log('result: ', result.key);
            this.db.object(`${this.baseTitleUrl}/${titulo.toLowerCase()}`).set(result.key)
              .then(
                response => {
                  resolve();
                }
              );
          }
        )
        .catch(
          error => {
            reject(error);
          }
        );
    });

  }

  update(articulo: Article) {
    const key = articulo.$key;
    delete articulo.$key;
    return new Promise((resolve, reject) => {
      if (!key) { reject('ocurrio un problema, intenta de nuevo') }
      this.db.object(`${this.baseUrl}/${key}`).set(articulo)
        .then(
          result => {
            const title = articulo.title.toLowerCase().replace(/ /g, '-');
            const oldTitle = articulo.oldTitle.toLowerCase().replace(/ /g, '-');
            this.db.object(`${this.baseTitleUrl}/${oldTitle}`).remove();
            this.db.object(`${this.baseTitleUrl}/${title}`).set(key)
              .then(
                response => {
                  resolve();
                }
              );
          }
        )
        .catch(
          error => {
            reject(error);
          }
        );
    });
  }

  remove(articulo: Article) {
    const key = articulo.$key;
    delete articulo.$key;
    const title = articulo.title.toLowerCase().replace(/ /g, '-');
    this.db.object(`${this.baseTitleUrl}/${title}`).remove();
    return this.db.object(`${this.baseUrl}/${key}`).remove();
  }

  checkTitle(title: string) {
    title = title.toLowerCase();
    return this.db.object(`${this.baseTitleUrl}/${title.replace(/ /g, '-')}`)
      //  tslint:disable-next-line:no-shadowed-variable
      .pipe(
        map(title => {
          return !title.$value;
        })
      )
  }
}
