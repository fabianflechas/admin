import { Component, OnInit } from '@angular/core';
import { Article } from '../../../core/model/article';
import { Observable } from 'rxjs';
import { DialogService } from '../../../core/components/dialog/dialog.service';
import { Router } from '@angular/router';
import { ArticleService } from '../article.service';
import { States } from '../../../core/model/states';

@Component({
  selector: 'app-article-list',
  templateUrl: './article-list.component.html',
  styleUrls: ['./article-list.component.scss']
})
export class ArticleListComponent implements OnInit {

  articles: Observable<Article[]>;

  constructor(
    private articleService: ArticleService,
    private dialogService: DialogService,
    private router: Router
  ) { }

  ngOnInit() {
    this.articles = this.articleService.getAll();
  }

  edit(article: Article) {
    this.router.navigate(['/formulario-articulo', { id: article.$key }]);
  }

  get States() {
    return States;
  }

  getStateName(state): String {
    // the state can be a string or a number .... horrible :/
    switch (+state) {
      case States.PUBLIC:
        return 'Publico';
      case States.PRIVATE:
        return 'Privado';
      case States.AUTH:
        return 'Autenticado';
      default:
        return 'Publico';
    }
  }
}
