import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { DialogService } from '../../../core/components/dialog/dialog.service';
import { Article } from '../../../core/model/article';
import { UploadService } from '../../../core/components/upload/upload.service';
import { Upload } from '../../../core/components/upload/upload';
import { ArticleService } from '../article.service';
import { States } from '../../../core/model/states';
import { take } from 'rxjs/operators';

declare var $;

@Component({
  selector: 'app-article-form',
  templateUrl: './article-form.component.html',
  styleUrls: ['./article-form.component.scss']
})
export class ArticleFormComponent implements OnInit {

  public article: Article = Article.createDefault();
  public create = true;
  public titleInvalid = false;
  photoLoaded = false;
  loadingMessage = '';

  constructor(
    private route: ActivatedRoute,
    private articleService: ArticleService,
    private dialogService: DialogService,
    private router: Router,
    private uploadService: UploadService
  ) {
  }

  ngOnInit() {
    // procesar el id pasado por parametro en la ruta
    this.route.paramMap
      .subscribe((params: ParamMap) => {
        // si hay un parametro id, es porque estamos editando un alumno
        if (params.get('id') != null) {
          this.create = false;
          // (+) before `params.get()` turns the string into a number
          console.log('get article: ', params.get('id'));
          this.articleService.get(params.get('id'))
            .pipe(
              take(1)
            )
            .subscribe(
              article => {
                console.log('llego el article: ', article);
                this.article = article;
                this.photoLoaded = this.article.photo !== null;
              },
              error => {
                this.dialogService.openDialog('error al cargar el article: ' + error.message, '', 'warning');
              }
            );
        } else { // si no hay un parametro es porque estamos creando el alumno
          this.create = true;
          this.article = Article.createDefault();
        }
      });
  }

  submit() {
    // tslint:disable-next-line:curly
    if (this.titleInvalid) return;
    console.log('guardando article: ', this.article);
    this.loadingMessage = 'guardando articulo, porfavor espere...';
    if (this.create) {
      this.articleService.save(this.article)
        .then(
          succss => {
            this.loadingMessage = '';
            this.dialogService.openDialog('el article se guardo con exito', '', 'success');
            this.router.navigate(['/articulos']);
          }
        )
        .catch(
          error => {
            this.loadingMessage = '';
            this.dialogService.openDialog('error al cargar el article: ' + error.message, '', 'warning');
          }
        );
    } else {
      this.articleService.update(this.article)
        .then(
          succss => {
            this.loadingMessage = '';
            this.dialogService.openDialog('el article se guardo con exito', '', 'success');
            this.router.navigate(['/articulos']);
          }
        )
        .catch(
          error => {
            this.loadingMessage = '';
            this.dialogService.openDialog('error al cargar el article: ' + error.message, '', 'warning');
          }
        );
    }
  }

  previewImage(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  private _handleReaderLoaded(e) {
    const reader = e.target;
    const blob = this.dataURItoBlob(reader.result);
    let upload = new Upload(<File>blob);
    this.uploadService.pushUpload(upload).subscribe(
      uploadChange => {
        upload = uploadChange;
        console.log('progreso: ', upload.progress, '%');
        // MOSTRAR EL PROGRESO EN EL HTML
      },
      error => {
        this.dialogService.openWarningDialog(error.message, '');
      },
      () => {
        this.article.photo = upload.url;
        this.photoLoaded = true;
      }
    );
    $('#preview').attr('src', reader.result);
  }

  removeArticle() {
    this.dialogService.openAskDialog('Esta seguro de eliminar este article?', '')
      .subscribe(
        confirm => {
          if (confirm) {
            this.articleService.remove(this.article);
            this.router.navigate(['/articulos']);
          }
        }
      );
  }

  contentChange(content: string) {
    console.log('content change');
    this.article.content = content;
  }

  private dataURItoBlob(dataURI) {
    const byteString = atob(dataURI.split(',')[1]);
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([ab], { type: mimeString });
    return blob;
  }

  verifyTitle() {
    // tslint:disable-next-line:curly
    if (this.article.title === this.article.oldTitle) return;
    this.articleService.checkTitle(this.article.title)
      .subscribe(
        success => {
          console.log('title verification.....', success);
          this.titleInvalid = !success;
        },
        error => {
          console.log('error al verificar titutlo');
        }
      );
  }

  get States() {
    return States;
  }

}
