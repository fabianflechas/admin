import { Component, OnInit } from '@angular/core';
import { AdminUsersService, IAdminUser } from '../admin-users.service';
import { AlertService } from '../../../core/components/alert/alert.service';
import { AuthenticationService } from '../../../core/authentication/authentication.service';
import { take } from 'rxjs/operators';

@Component({
  selector: 'app-admin-associated-form',
  templateUrl: './admin-associated-form.component.html',
  styleUrls: ['./admin-associated-form.component.css']
})
export class AdminAssociatedFormComponent implements OnInit {

  //  adminForm: FormGroup;
  loading: boolean = false;

  privateData = null; // data that retive from the database
  admin: IAdminUser; // admin interface
  uid: string = ''; // uid del usurio que se va a registrar
  permission: number = 0; // permisos que tendra el usuario
  disabled: boolean = false; // desavilita para buscar en la base de datos

  constructor(
    private adminUserService: AdminUsersService,
    private alertService: AlertService,
    private authService: AuthenticationService
  ) {

  }

  ngOnInit() {
  }

  setTypeOfUser(value) {
    this.permission = value;
  }

  public cancelUser() {
    this.privateData = null;
    this.uid = '';
  }

  saveAdmin() {
    if (this.validate()) {
      this.loading = true;
      this.alertService.showInfoAlert('Guardando nuevo administrador', true);
      this.admin = {
        displayName: this.privateData.displayName,
        email: this.privateData.email,
        permission: this.permission,
        timestamp: Date.now().toString()
      }
      this.adminUserService.createAdminUserWithUid(this.admin, this.uid)
        .then(response => {
          this.loading = false;
          this.alertService.showSuccessAlert(
            `Se a creado el usuario ${this.admin.displayName}, UID:${this.uid}`,
            false,
            5000)
          this.privateData = null;
          this.uid = '';
        })
        .catch(error => {
          this.loading = false;
          this.alertService.showErrorAlert(error.message, false);
        })
    }
  }

  public obtenerUser(event) {
    this.disabled = true;
    this.alertService.showInfoAlert('Buscando usuario, espera un momento', true);
    this.authService.getUserWithUid(this.uid)
      .pipe(
        take(1)
      )
      .subscribe(privateData => {
        this.disabled = false
        this.privateData = privateData;
        if (privateData) {
          this.alertService.showSuccessAlert('encontrado', false, 4000);
        }
        else {
          this.alertService.showErrorAlert('NO encontrado', false);
        }
      })
  }

  public validate() {
    if (this.uid.length == 0) {
      this.alertService.showErrorAlert('ingresa un UID (user id)', false);
      return false;
    }
    if (!this.privateData) {
      this.alertService.showErrorAlert('Asigna un UID valido', false)
      return false
    }
    if (this.permission == 0) {
      this.alertService.showErrorAlert('Selecciona un permiso', false);
      return false;
    }
    return true;
  }



}
