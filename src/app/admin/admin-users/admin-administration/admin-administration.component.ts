import { Component, OnInit } from '@angular/core';
import { AdminUsersService, IAdminUser } from '../admin-users.service';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-admin-administration',
  templateUrl: './admin-administration.component.html',
  styleUrls: ['./admin-administration.component.scss']
})
export class AdminAdministrationComponent implements OnInit {

  adminUsers : Observable<IAdminUser[]>;

  constructor(
    private adminUsersService: AdminUsersService) {

    }

  ngOnInit() {
    this.adminUsers = this.adminUsersService.getAdminList()
  }

}
