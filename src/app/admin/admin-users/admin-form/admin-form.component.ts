import { Component, OnInit } from '@angular/core';
//  import { AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators } from '@angular/forms';
import { AdminUsersService, IAdminUser } from '../admin-users.service';
import { AlertService } from '../../../core/components/alert/alert.service';

declare var $;

@Component({
  selector: 'app-admin-form',
  templateUrl: './admin-form.component.html',
  styleUrls: ['./admin-form.component.scss']
})
export class AdminFormComponent implements OnInit {
  //  adminForm: FormGroup;
  loading: boolean = false;

  public admin: IAdminUser;
  public password: string = '';
  public confirmPassword: string = '';
  public disbled: boolean = false;

  constructor(
    private adminUserService: AdminUsersService,
    private alertService: AlertService
    //  private formBuilder: FormBuilder
  ) {

  }

  ngOnInit() {
    this.admin = {
      displayName: '',
      email: '',
      permission: 0,
      timestamp: ''
    }
  }

  setTypeOfUser(value) {
    this.admin.permission = value;
  }

  public saveAdmin() {
    if (this.validate()) {
      this.loading = true;
      this.alertService.showInfoAlert('Guardando nuevo administrador', true);
      this.admin.timestamp = Date.now().toString();
      this.adminUserService.createAdminUser(this.admin,this.password)
      .then(response =>{
        this.loading = false;
        this.alertService.showSuccessAlert(
          `Se a creado el usuario ${this.admin.displayName}, UID:${response.uid}`,
          false,
          5000)
      })
      .catch(error =>{
        this.loading = false;
        this.alertService.showErrorAlert(error,false);
      }) 
    }
  }

  public validate() {
    if (this.admin.displayName.length == 0) {
      this.alertService.showErrorAlert('ingresa el nombre', false);
      return false;
    }
    if (this.admin.email.length == 0) {
      this.alertService.showErrorAlert('ingresa el correo', false);
      return false;
    }

    if (this.password.length < 8) {
      this.alertService.showErrorAlert('la clave es muy corta', false);
      return false;
    }
    if (this.confirmPassword != this.password) {
      this.alertService.showErrorAlert('las claves son distintas', false);
      return false;
    }
    if (this.admin.permission == 0) {
      this.alertService.showErrorAlert('Selecciona un permiso', false);
      return false;
    }
    return true;
  }



}
