import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';
import { AngularFireAuth } from 'angularfire2/auth';
import { AuthenticationService } from '../../core/authentication/authentication.service';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

export interface IAdminUser {
  displayName: string;
  email: string;
  permission: number;
  timestamp: string;
  uid?: string;
}

@Injectable()
export class AdminUsersService {

  constructor(private database: AngularFireDatabase,
    private auth: AngularFireAuth,
    private authService: AuthenticationService) { }

  public createAdminUser(admin: IAdminUser, password): Promise<any> {
    return new Promise((resolve, reject) => {
      this.auth.auth.createUserWithEmailAndPassword(admin.email, password)
        .then(succes => {
          this.database.object(`/Admins/${succes.uid}`).set(admin)
            .then(saved => { resolve({ uid: succes.uid }) })
            .catch(error => { reject(error) })
        })
        .catch(error => {
          reject(error);
        })
    })
  }

  public createAdminUserWithUid(admin: IAdminUser, uid) {
    return this.database.object(`/Admins/${uid}`).set(admin)
  }

  public getAdminList(): Observable<IAdminUser[]> {
    return this.database.list('Admins')
      .pipe(
        map(list => {
          return list.map(item => {
            return {
              displayName: item.displayName,
              email: item.email,
              permission: item.permission,
              timestamp: item.timestamp,
              uid: item.$key
            }
          })
        })
      )
  }

  //  public getAdmin(uid){
  //    // this.database.object(`Admins/${uid}`)
  //  }


}
