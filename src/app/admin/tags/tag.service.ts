import { AngularFireDatabase } from 'angularfire2/database';
import { Observable } from 'rxjs';
import { Tag } from '../../core/model/tag';
import { map } from 'rxjs/operators';
import { ITag } from '../../core/model/ITag';
import { ITagItem } from '../../core/model/ITagItem';

export abstract class TagService {

  abstract baseUrl: string;
  abstract baseItemUrl: string;
  abstract baseDefaultTagUrl: string;
  abstract baseTitleUrl: string;

  constructor(
    private db: AngularFireDatabase
  ) { }

  // obtener todos los tags
  getAll(): Observable<ITag[]> {
    return this.db.list(`${this.baseUrl}`, {
      query: {
        orderByChild: 'name'
      }
    })
      .pipe(
        map(result => {
          return result.map(tag => {
            let groups = [];
            // get groups from tag
            for (var key in tag.groups) {
              if (tag.groups.hasOwnProperty(key)) {
                let group = { ...tag.groups[key], $key: key };
                groups.push(group);
              }
            }
            tag.groups = groups;
            return tag;
          })
        })
      )
  }

  get(key): Observable<ITag> {
    return this.db.object(`${this.baseUrl}/${key}`);
  }

  getDefaultTagKey(): Observable<string> {
    return this.db.object(`${this.baseDefaultTagUrl}`)
      .pipe(
        map(res => {
          return res.$value
        })
      )
  }

  setDefaultTagKey(key) {
    return this.db.object(`${this.baseDefaultTagUrl}`).set(key);
  }

  save(tag: ITag) {
    console.log("saving tag");
    delete tag.$key;
    let groups = tag.groups;
    delete tag.groups;
    return new Promise((resolve, reject) => {
      this.db.list(`${this.baseUrl}`).push(tag)
        .then(
          result => {
            groups.forEach(group => {
              this.db.list(`${this.baseUrl}/${result.key}/groups`).push(group);
            })
            let titulo = tag.name.replace(/ /g, '-');
            this.db.object(`${this.baseTitleUrl}/${titulo.toLowerCase()}`).set(result.key)
              .then(
                () => {
                  resolve();
                }
              )
          }
        )
        .catch(
          error => {
            reject(error);
          }
        )
    });
  }

  update(tag: ITag, removedGroupsKeys: string[]) {
    let key = tag.$key;
    delete tag.$key;
    return new Promise((resolve, reject) => {

      // a veces guardaba con key undefined, solucion rapido
      if (!key) return;

      //update groups
      tag.groups.forEach(group => {

        // update existing groups
        if (group.$key) {
          let groupKey = group.$key;
          delete group.$key;
          this.db.object(`${this.baseUrl}/${key}/groups/${groupKey}`).update(group);
        }
        //add new groups
        else {
          delete group.$key;
          this.db.list(`${this.baseUrl}/${key}/groups`).push(group);
        }

      });

      //remove groups
      this.removeGroups(
        key,
        removedGroupsKeys,
        Object.keys(tag.items || [])
          .map(key => { return { ...tag.items[key], $key: key } })
      );

      // prevent groups and items to be updated
      delete tag.groups;
      delete tag.items;
      //to avoid set oldName property in firebase
      let oldName = tag.oldName;
      delete tag.oldName;
      this.db.object(`${this.baseUrl}/${key}`).update(tag)
        .then(
          result => {
            //check if the name has changed
            if (oldName !== tag.name) {
              let title = tag.name.toLowerCase().replace(/ /g, '-');
              let oldTitle = oldName.toLowerCase().replace(/ /g, '-');
              this.db.object(`${this.baseTitleUrl}/${oldTitle}`).remove();
              this.db.object(`${this.baseTitleUrl}/${title}`).set(key)
                .then(
                  response => {
                    resolve();
                  }
                )
            } else {
              resolve();
            }
          }
        )
        .catch(
          error => {
            reject(error);
          }
        )
    });
  }

  removeGroups(tagKey: string, removedGroupsKeys: string[], items: ITagItem[]) {

    //remove groups from items
    if (items) {
      items.forEach(item => {

        //check for deleted groups
        item.groups.forEach((currentGroupKey: string, index: number) => {

          //delete group from tag item
          if (removedGroupsKeys.indexOf(currentGroupKey) >= 0) {
            this.db.object(`${this.baseUrl}/${tagKey}/items/${item.$key}/groups/${currentGroupKey}`).remove();
            //TODO: change for item base url
            this.db.object(`${this.baseItemUrl}/${item.$key}/tags/${tagKey}/groups/${index}`).remove();
          }

        });
      })
    }

    //remove groups from tag list
    if (removedGroupsKeys) {
      removedGroupsKeys.forEach(groupKey => {
        this.db.object(`${this.baseUrl}/${tagKey}/groups/${groupKey}`).remove();
      });
    }

  }

  remove(tag: ITag) {
    let key = tag.$key;
    delete tag.$key;
    return new Promise((resolve, reject) => {

      // eliminar la referencia en los items
      Object.keys(tag.items || [])
        .map(key => { return { ...tag.items[key], $key: key } })
        .forEach((item: ITagItem) => {
          this.db.object(`${this.baseItemUrl}/${item.$key}/tags/${key}`).remove();
        });

      // eliminar titulo
      this.db.object(`${this.baseTitleUrl}/${tag.name}`).remove();

      // eliminar tag
      this.db.object(`${this.baseUrl}/${key}`).remove()
        .then(a => {
          resolve();
        })
        .catch(error => reject(error))

    });
  }

  checkName(name: string) {
    name = name.toLowerCase();
    return this.db.object(`${this.baseTitleUrl}/${name.replace(/ /g, '-')}`)
      .pipe(
        map(title => {
          return !title.$value
        })
      )
  }

  getTags(tagsKeys) {
    console.log('tags keys', tagsKeys);
    let tags = [];

    return new Promise((resolve, reject) => {
      const tagsPromises = tagsKeys.map(key => {
        this.db.object(`${this.baseUrl}/${key}`)
          .pipe(
            map(Tag.fromJson)
          )
          .subscribe(
            tag => {
              delete tag.videos;
              delete tag.gifts;
              tags.push(tag)
            }
          )
      });

      Promise.all(tagsPromises)
        .then(a => {
          resolve(tags)
        })
    })
  }

}
