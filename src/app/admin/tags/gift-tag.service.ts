import { Injectable } from '@angular/core';
import { TagService } from './tag.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class GiftTagService extends TagService {

  baseUrl: string = "/TagsGift";
  baseItemUrl: string = "/Gifts";
  baseDefaultTagUrl: string = "/TagsGiftDefaultKey";
  baseTitleUrl: string = '/Titles/tags-gift';

  constructor(
    db: AngularFireDatabase
  ) {
    super(db);
  }

}
