import { Component, OnInit, Injector } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { TagService } from '../tag.service';
import { DialogService } from '../../../core/components/dialog/dialog.service';
import { take } from 'rxjs/operators';
import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { ITag } from '../../../core/model/ITag';
import { IGroup } from '../../../core/model/IGroup';
import { VideoTagService } from '../video-tag.service';
import { GiftTagService } from '../gift-tag.service';
import { UploadService } from '../../../core/components/upload/upload.service';
import { Upload } from '../../../core/components/upload/upload';
import { ITagItem } from '../../../core/model/ITagItem';

declare var $;

@Component({
  selector: 'app-tag-form',
  templateUrl: './tag-form.component.html',
  styleUrls: ['./tag-form.component.scss']
})
export class TagFormComponent implements OnInit {

  tag: ITag;
  tagService: TagService;
  tagType;

  create: boolean = true;
  nameInvalid: boolean = false;

  newGroupName = '';
  // to store the groups list temporary
  groups: IGroup[];
  // to store removed groups (if they already exist in firebase)
  removedGroupsKeys: string[] = [];

  imageProgress: number = null;

  constructor(
    private injector: Injector,
    private route: ActivatedRoute,
    private dialogService: DialogService,
    private router: Router,
    private uploadService: UploadService
  ) {
  }

  ngOnInit() {
    this.tagType = this.route.snapshot.data['tagType'];
    switch (this.tagType) {
      case 'video':
        this.tagService = <VideoTagService>this.injector.get(VideoTagService);
        break;
      case 'gift':
        this.tagService = <GiftTagService>this.injector.get(GiftTagService);
        break;
    }

    // procesar el id pasado por parametro en la ruta
    this.route.paramMap
      .subscribe((params: ParamMap) => {
        // si hay un parametro id, es porque estamos editando un tag
        if (params.get('id') != null) {
          this.create = false;
          //  (+) before `params.get()` turns the string into a number
          this.tagService.get(params.get('id'))
            .pipe(
              take(1)
            )
            .subscribe(
              tag => {
                console.log('llego el tag: ', tag);
                this.imageProgress = 0;
                this.tag = tag;
                this.groups = [];
                // map te groups
                Object.keys(tag.groups).forEach(groupKey => {
                  this.groups.push({ ...tag.groups[groupKey], $key: groupKey });
                });
                this.groups.sort((a, b) => a.order - b.order);
                // assign old name
                this.tag.oldName = this.tag.name;
              },
              error => {
                this.dialogService.openDialog('error al cargar el tag: ' + error.message, '', 'warning');
              }
            )
        } else { // si no hay un parametro es porque estamos creando el tag
          this.create = true;
          this.tag = {
            groups: [],
            name: '',
          }
          this.groups = [];
        }
      });
  }

  submit() {
    if (this.nameInvalid) return;
    this.tag.groups = this.groups;
    if (this.create) {
      this.tagService.save(this.tag)
        .then(
          succss => {
            this.dialogService.openDialog('el tag se guardo con exito', '', 'success');
            this.router.navigate([`/${this.tagType}-tags`]);
            this.groups = [];
          }
        )
        .catch(
          error => {
            this.dialogService.openDialog('error al cargar el tag: ' + error, '', 'warning');
          }
        )
    } else {
      this.tagService.update(this.tag, this.removedGroupsKeys)
        .then(
          succss => {
            this.dialogService.openDialog('el tag se guardo con exito', '', 'success');
            this.router.navigate([`/${this.tagType}-tags`]);
            this.groups = [];
          }
        )
        .catch(
          error => {
            this.dialogService.openDialog('error al cargar el tag: ' + error, '', 'warning');
          }
        );
      this.removedGroupsKeys = [];
    }
  }

  removeTag() {
    this.dialogService.openAskDialog('Esta seguro de eliminar este tag?', '')
      .subscribe(
        confirm => {
          if (confirm) {
            this.tagService.remove(this.tag);
            this.router.navigate([`/${this.tagType}-tags`]);
          }
        }
      );
  }

  drop(event: CdkDragDrop<string[]>) {
    moveItemInArray(this.groups, event.previousIndex, event.currentIndex);
    this.groups.forEach((group, index) => {
      group.order = index;
    });
  }

  addGroup() {
    this.groups.push({
      name: this.newGroupName,
      order: this.groups.length
    });
    this.newGroupName = "";
  }

  removeGroup(index) {
    let group = this.groups[index];
    // if the group already exist add to the list of removed
    if (group.$key) {
      this.removedGroupsKeys.push(group.$key);
    }
    this.groups.splice(index, 1);
    this.groups.forEach((group, index) => {
      group.order = index;
    });
  }

  groupNameInvalid(name) {
    return this.groups.length > 0 ? this.groups.find(group => group.name == name) !== undefined : false;
  }

  verifyName() {
    if (this.tag.name == this.tag.oldName) return;
    this.tagService.checkName(this.tag.name)
      .subscribe(
        success => {
          this.nameInvalid = !success;
        },
        error => {
          console.log('error al verificar titutlo');
        }
      )
  }

  previewImage(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  private _handleReaderLoaded(e) {
    const reader = e.target;
    const blob = this.dataURItoBlob(reader.result);
    let upload = new Upload(<File>blob);
    this.uploadService.pushUpload(upload).subscribe(
      uploadChange => {
        upload = uploadChange;
        // Progress
        this.imageProgress = upload.progress;
      },
      error => {
        this.dialogService.openWarningDialog(error.message, '');
      },
      () => {
        this.tag.photo = upload.url;
      }
    );
    $('#preview').attr('src', reader.result);
  }

  private dataURItoBlob(dataURI) {
    const byteString = atob(dataURI.split(',')[1]);
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([ab], { type: mimeString });
    return blob;
  }

}
