import { Component, OnInit, Injector } from '@angular/core';
import { Tag } from '../../../core/model/tag';
import { Observable } from 'rxjs';
import { TagService } from '../tag.service';
import { Router, ActivatedRoute } from '@angular/router';
import { ITag } from '../../../core/model/ITag';
import { VideoTagService } from '../video-tag.service';
import { GiftTagService } from '../gift-tag.service';
import { take } from 'rxjs/operators';
import { DialogService } from '../../../core/components/dialog/dialog.service';

@Component({
  selector: 'app-tag-list',
  templateUrl: './tag-list.component.html',
  styleUrls: ['./tag-list.component.scss']
})
export class TagListComponent implements OnInit {

  tags: Observable<ITag[]>;
  tagService: TagService;
  tagType;
  defaultTagKey;

  constructor(
    private injector: Injector,
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: DialogService,
  ) { }

  ngOnInit() {
    this.tagType = this.route.snapshot.data['tagType'];
    switch (this.tagType) {
      case 'video':
        this.tagService = <VideoTagService>this.injector.get(VideoTagService);
        break;
      case 'gift':
        this.tagService = <GiftTagService>this.injector.get(GiftTagService);
        break;
    }

    this.tags = this.tagService.getAll();
    this.tagService.getDefaultTagKey()
      .pipe(
        take(1)
      )
      .subscribe(res => this.defaultTagKey = res)

  }

  edit(tag: Tag) {
    this.router.navigate([`/formulario-${this.tagType}-tag`, { id: tag.$key }]);
  }

  changeDefaultTagKey() {
    this.tagService.setDefaultTagKey(this.defaultTagKey)
      .then(res => this.dialogService.openSuccessDialog('exito al guardar tag por defecto', 'exito'))
      .catch(err => this.dialogService.openWarningDialog(`error al guardar tag por defecto, error: ${err}`, 'error'))
  }

}
