import { Injectable } from '@angular/core';
import { TagService } from './tag.service';
import { AngularFireDatabase } from 'angularfire2/database';

@Injectable({
  providedIn: 'root'
})
export class VideoTagService extends TagService {

  baseUrl: string = "/TagsVideo";
  baseItemUrl: string = "/Videos";
  baseDefaultTagUrl: string = "/TagsVideoDefaultKey";
  baseTitleUrl: string = '/Titles/tags-video'

  constructor(
    db: AngularFireDatabase
  ) {
    super(db);
  }
}
