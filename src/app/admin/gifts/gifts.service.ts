import { Injectable } from '@angular/core';
import { ItemService } from '../items/item.service';
import { IGift } from '../../core/model/IGift';
import { AngularFireDatabase } from 'angularfire2/database';


@Injectable()
export class GiftsService extends ItemService<IGift> {

  baseUrl: string = '/Gifts';
  baseTagUrl: string = '/TagsGift';
  baseTitleUrl: string = '/Titles/gifts';

  constructor(
    _db: AngularFireDatabase
  ) {
    super(_db)
  }

}
