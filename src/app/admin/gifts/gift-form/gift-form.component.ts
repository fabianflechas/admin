import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { DialogService } from '../../../core/components/dialog/dialog.service';
import { UploadService } from '../../../core/components/upload/upload.service';
import { Upload } from '../../../core/components/upload/upload';
import { States } from '../../../core/model/states';
import { GiftTypes } from '../../../core/model/gift-types';
import { IGift } from '../../../core/model/IGift';
import { GiftTagService } from '../../tags/gift-tag.service';
import { take } from 'rxjs/operators';
import { GiftsService } from '../gifts.service';
import { BaseItemForm } from '../../items/base-item-form';

declare var $;

@Component({
  selector: 'app-gift-form',
  templateUrl: './gift-form.component.html',
  styleUrls: ['./gift-form.component.scss', '../../items/base-item-form.scss']
})
export class GiftFormComponent extends BaseItemForm<IGift> implements OnInit {

  gift: IGift;
  create = true;
  typeDescription = {};
  photoLoaded = false;

  constructor(
    private route: ActivatedRoute,
    private giftService: GiftsService,
    private dialogService: DialogService,
    private router: Router,
    private uploadService: UploadService,
    private tagService: GiftTagService
  ) {
    super(giftService);

    this.typeDescription[GiftTypes.LINK] = 'Link al regalo';
    this.typeDescription[GiftTypes.STORY] = 'Link a la historia';
    this.typeDescription[GiftTypes.GAME] = 'Iframe del juego';
    this.typeDescription[GiftTypes.WIMIGAME] = 'Ingresa el nombre de la carpeta que contiene juego';
  }

  ngOnInit() {

    this.tagService.getAll().subscribe(
      tags => {
        this.tags = tags.map(tag => {
          delete tag.items;
          return tag;
        });
        this.getGift();
      }
    );


  }

  getGift() {
    // procesar el id pasado por parametro en la ruta
    this.route.paramMap
      .subscribe((params: ParamMap) => {
        // si hay un parametro id, es porque estamos editando un gift
        if (params.get('id') != null) {
          this.create = false;
          // (+) before `params.get()` turns the string into a number
          this.giftService.get(params.get('id'))
            .pipe(
              take(1)
            )
            .subscribe(
              gift => {
                this.gift = gift;
                this.gift.oldTitle = this.gift.title;
                this.photoLoaded = true;
                // mapea del formato guardado en la base de datos a un formato que se puede usar en el form
                this.selectedTags = this.mapToFormTags(gift);
                this.deletedTagsKeys = [];
              },
              error => {
                this.dialogService.openDialog('error al cargar el gift: ' + error.message, '', 'warning');
              }
            );
        } else {
          // si no hay un parametro es porque estamos creando el Gift
          this.create = true;
          this.selectedTags, this.deletedTagsKeys = [];
          this.gift = {
            bills: 0,
            coins: 0,
            content: '',
            date: Date.now(),
            description: '',
            photo: null,
            state: States.PUBLIC,
            title: '',
            type: null
          };
        }
      });
  }

  submit() {
    if (this.titleInvalid) return;
    this.gift.tags = this.mapToDatabseTags();
    if (this.create) {
      this.giftService.save(this.gift)
        .then(
          succss => {
            this.dialogService.openDialog('el gift se guardo con exito', '', 'success');
            this.router.navigate(['/regalos']);
          }
        )
        .catch(
          error => {
            this.dialogService.openDialog('error al cargar el gift: ' + error, '', 'warning');
          }
        );
    } else {
      this.giftService.update(this.gift, this.deletedTagsKeys)
        .then(
          succss => {
            this.dialogService.openDialog('el gift se guardo con exito', '', 'success');
            this.router.navigate(['/regalos']);
          }
        )
        .catch(
          error => {
            this.dialogService.openDialog('error al cargar el gift: ' + error, '', 'warning');
          }
        );
    }
  }

  removeGift() {
    this.dialogService.openAskDialog(
      `Esta seguro de eliminar este "gift" ? recuerda que si lo haces se
       eliminara de todos los niños que lo tengan en su perfil`, '')
      .subscribe(
        confirm => {
          if (confirm) {
            this.giftService.remove(this.gift);
            this.router.navigate(['/gifts']);
          }
        }
      );
  }

  get GiftTypes() {
    return GiftTypes;
  }

  previewImage(e) {
    const file = e.dataTransfer ? e.dataTransfer.files[0] : e.target.files[0];
    const reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
    reader.readAsDataURL(file);
  }

  private _handleReaderLoaded(e) {
    const reader = e.target;
    const blob = this.dataURItoBlob(reader.result);
    // file extends blob.
    let upload = new Upload(<File>blob);
    this.uploadService.pushUpload(upload).subscribe(
      uploadChange => {
        upload = uploadChange;
        console.log('progreso: ', upload.progress, '%');
        // MOSTRAR EL PROGRESO EN EL HTML
      },
      error => {
        this.dialogService.openWarningDialog(error.message, '');
      },
      () => {
        this.gift.photo = upload.url;
        this.photoLoaded = true;
      }
    );
    $('#preview').attr('src', reader.result);
  }

  private dataURItoBlob(dataURI) {
    const byteString = atob(dataURI.split(',')[1]);
    const mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];
    const ab = new ArrayBuffer(byteString.length);
    const ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    const blob = new Blob([ab], { type: mimeString });
    return blob;
  }

}
