import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';
import { GiftsService } from '../gifts.service';
import { States } from '../../../core/model/states';
import { IGift } from '../../../core/model/IGift';

@Component({
  selector: 'app-gift-list',
  templateUrl: './gift-list.component.html',
  styleUrls: ['./gift-list.component.scss']
})
export class GiftListComponent implements OnInit {

  gifts: Observable<IGift[]>;

  constructor(
    private giftService: GiftsService,
    private router: Router
  ) { }

  ngOnInit() {
    this.gifts = this.giftService.getAll();
  }

  edit(regalo: IGift) {
    this.router.navigate(['/formulario-regalo', { id: regalo.$key }]);
  }

  get States() {
    return States;
  }

  getStateName(state): String {
    // the state can be a string or a number .... horrible :/
    switch (+state) {
      case States.PUBLIC:
        return 'Publico';
      case States.PRIVATE:
        return 'Privado';
      case States.AUTH:
        return 'Autenticado';
      default:
        return 'Publico';
    }
  }
}
