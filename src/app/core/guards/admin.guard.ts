import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable ,  Subject } from 'rxjs';
import { AuthenticationService } from '../authentication/authentication.service';

@Injectable()
export class AdminGuard implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    let response: Subject<boolean> = new Subject();
    this.authenticationService.checkAdmin()
      .subscribe(allowed => {
        if (!allowed) {
          console.log('no es admin');
          response.next(false);
          this.router.navigate(['/signin-admin']); // tiene que ir al login
        } else {
          response.next(true);
          console.log('es admin');
        }
      });
    return response;
  }

}
