import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';



// services
import { AuthenticationService } from '../authentication/authentication.service';
import { take, tap } from 'rxjs/operators';

@Injectable()
export class AuthenticationGuard implements CanActivate {

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
    return this.authenticationService.isAuthenticated()
      .pipe(
        take(1),
        tap(allowed => {
          if (!allowed) {
            this.router.navigate(['/signin-admin']); // tiene que ir al login
          }
        })
      )
  }
}
