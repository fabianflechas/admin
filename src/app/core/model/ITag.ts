import { IGroup } from './IGroup';
import { ITagItem } from './ITagItem';

export interface ITag {
  $key?: string;
  name: string;
  oldName?: string;
  groups: IGroup[];
  items?: ITagItem[];
  photo?: string;
}
