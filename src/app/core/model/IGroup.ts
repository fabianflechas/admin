export interface IGroup {
    $key?: string;
    name: string;
    order?: number;
}