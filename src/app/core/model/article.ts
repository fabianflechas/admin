import { States } from './states';
export class Article {

  constructor(
    public $key: string,
    public title: string,
    public oldTitle: string,
    public description: string,
    public video: string,
    public photo: string,
    public content: string,
    public date: number,
    public state: number,
  ) { }

  static fromJson({ $key, title, description, video, photo, content, date, state }): Article {
    return new Article(
      $key,
      title,
      title,
      description,
      video,
      photo,
      content,
      date,
      state,
    );
  }

  static fromJsonArray(array: any): Article[] {
    return array.map(Article.fromJson);
  }

  static createDefault(): Article {
    return new Article('', '', '', '', '', '', '', Date.now(), States.PRIVATE);
  }
}
