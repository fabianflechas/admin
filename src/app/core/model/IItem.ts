import { States } from './states';

export interface IItem {

    $key?: string;
    title: string;
    oldTitle?: string;
    tags?: any;
    description: string;
    state: States;

}