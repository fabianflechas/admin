import { GiftTypes } from './gift-types';
import { IItem } from './IItem';

export interface IGift extends IItem {

  photo: string;
  coins: number;
  bills: number;
  date: number;
  type: GiftTypes;
  content: string;

}
