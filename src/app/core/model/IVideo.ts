import { IItem } from './IItem';

export interface IVideo extends IItem {

  date: number;
  link: string;

}
