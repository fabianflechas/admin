export class Tag {

  constructor(
    public $key: string,
    public name: string,
    public oldName: string,
    public gifts: any[],
    public videos: any[],
    public photo: string,
  ) { }

  static fromJson({ $key, name, gifts, videos, photo }): Tag {
    return new Tag(
      $key,
      name,
      name,
      gifts,
      videos,
      photo
    );
  }

  static fromJsonArray(array: any): Tag[] {
    return array.map(Tag.fromJson);
  }

  static createDefault(): Tag {
    return new Tag('', '', '', [], [], '');
  }
}
