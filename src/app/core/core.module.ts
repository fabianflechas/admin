import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';

// firebase conection
import { environment } from '../../environments/environment';
import { FormsModule, ReactiveFormsModule } from '@angular/forms'; //  forms
import { AngularFireModule } from 'angularfire2'; // firebase
import { AngularFireDatabaseModule } from 'angularfire2/database'; // firebase
import { AngularFireAuthModule } from 'angularfire2/auth';//  firebase

// servicios
import { AuthenticationService } from './authentication/authentication.service';
import { AlertService } from './components/alert/alert.service';
import { ImgLoaderService } from './components/img-loading/img-loader.service';
import { LoadingService } from './components/loading/loading.service';

// guardas
import { AdminGuard } from './guards/admin.guard';
import { AuthenticationGuard } from './guards/authentication.guard';

// components
import { ImgLoadingComponent } from './components/img-loading/img-loading.component';
import { SigninAdminComponent } from './components/signin-admin/signin.component';
import { DialogComponent } from './components/dialog/dialog.component';

//  firebase
import * as firebase from 'firebase';
import { AlertComponent } from './components/alert/alert.component';
import { UploadService } from './components/upload/upload.service';
import { DialogService } from './components/dialog/dialog.service';
import { BigComponent } from './components/loading/big/big.component';

firebase.initializeApp(environment.firebase);

// aca se importan modulos y servicios comunes a todos los demas modules
// por ejemplo el servicio de authenticacion o un modulo de un tercero que se use en toda la app
@NgModule({
  imports: [
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule, //  imports firebase/database, only needed for database features
    AngularFireAuthModule,  //  imports firebase/auth, only needed for auth features
    CommonModule,
    RouterModule,
    ReactiveFormsModule
  ],
  declarations: [
    DialogComponent,
    ImgLoadingComponent,
    AlertComponent,
    SigninAdminComponent,
    BigComponent
  ],
  exports: [
    DialogComponent,
    ImgLoadingComponent,
    AlertComponent,
    SigninAdminComponent
  ]
})
export class CoreModule {
  static forRoot() {
    return {
      ngModule: CoreModule,
      providers: [
        AuthenticationGuard,
        AuthenticationService,
        AlertService,
        ImgLoaderService,
        LoadingService,
        AdminGuard,
        UploadService,
        DialogService
      ]
    };
  }
}
