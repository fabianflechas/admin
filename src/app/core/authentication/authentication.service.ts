import { Injectable } from '@angular/core';
import { AngularFireAuth } from 'angularfire2/auth';
import * as firebase from 'firebase/app';
import { AngularFireDatabase } from 'angularfire2/database';
import { Subject } from 'rxjs/Subject';
import { take, map } from 'rxjs/operators';
import { Observable } from 'rxjs';


@Injectable()
export class AuthenticationService {

  constructor(
    private afAuth: AngularFireAuth,
    private bd: AngularFireDatabase
  ) { }

  isAuthenticated(): Observable<boolean> {
    // this.afAuth.authState.subscribe(state => console.log('estado',state));
    return this.afAuth.authState
      .pipe(
        map(val => { return val != null }) // por ahora no se verifica
      )
  }

  getCurrentUser(): any {
    return this.afAuth.authState;
  }

  getUserWithUid(uid): Observable<any> {
    return this.bd.object(`/Users/${uid}/privateData`)
      .pipe(
        map(x => {
          return x.email ? { displayName: `${x.lastname}, ${x.name}`, email: x.email } : null;
        })
      )
  }

  getCurrentUserId(): string {
    return this.afAuth.auth.currentUser ? this.afAuth.auth.currentUser.uid : null;
  }

  getCurrentId(): string {
    return this.afAuth.auth.currentUser.uid;
  }

  logInWithEmail(email: string, password: string): Promise<any> {
    return new Promise((resolve, reject) => {
      this.afAuth.auth.signInWithEmailAndPassword(email, password)
        .then(
          succs => {
            resolve()
          }
        )
        .catch(
          error => {
            reject(error);
          }
        )
    })
  }

  logout() {
    return this.afAuth.auth.signOut();
  }

  checkAdmin(): Observable<boolean> {
    let response: Subject<boolean> = new Subject();
    this.isAuthenticated()
      .pipe(
        take(1)
      )
      .subscribe(allowed => {
        if (!allowed) {
          response.next(false);
          response.complete();
        } else {
          this.bd.object(`/Admins/${this.getCurrentUserId()}`)
            .subscribe(
              usr => {
                if (usr.permission == null) {
                  response.next(false);
                  response.complete();
                } else {
                  response.next(true);
                  response.complete();
                }
              }
            )
        }
      });
    return response.asObservable();
  }

  getAdminUser() {
    return this.bd.object(`/Admins/${this.getCurrentUserId()}`);
  }

}

