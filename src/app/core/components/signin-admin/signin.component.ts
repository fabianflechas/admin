import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {AuthenticationService} from '../../authentication/authentication.service';
import {DialogService} from '../dialog/dialog.service';

@Component({
  selector: 'app-signin-admin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss']
})
export class SigninAdminComponent implements OnInit {

  form: FormGroup;
  loading: boolean = false;

  constructor(
    private fb: FormBuilder,
    private authService: AuthenticationService,
    private dialogService : DialogService,
    private router : Router
  ) { }

  ngOnInit() {

    console.log('llego al signin admin');

    this.form = this.fb.group({
      email: ['', Validators.compose([
        Validators.required
      ])],
      password: ['',Validators.required]
    });
  }

  isValid(campo:string){
    return this.form.controls[campo].invalid && this.form.controls[campo].dirty && this.form.controls[campo].touched;
  }

  onSubmit(){
    // si el formulario es valido entonces se guardan los campos
    if(this.form.valid){
      let formValue = this.form.value;
      this.loading = true;
      this.authService.logInWithEmail(formValue.email, formValue.password)
        .then(
          succss => {
            this.router.navigate(['/admin']);
            this.loading = false;
          }
        )
        .catch(
          error => {
            console.log(error);
            this.loading = false;
            this.dialogService.openDialog('ups! ocurrio un error al intentar iniciar sesion', '', 'warning');
          }
        )

    }else{ // si no es valido fuerza a que se muestren los mensajes de error
      for (let controlKey in this.form.controls) {
        this.form.controls[controlKey].markAsDirty();
        this.form.controls[controlKey].markAsTouched();
      }
    }
  }

  checkIfEmailInString(text) {
    let re = /(([^<>()[\]\\.,;:\s@\']+(\.[^<>()[\]\\.,;:\s@\']+)*)|(\'.+\'))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))/;
    return re.test(text);
  }

}
