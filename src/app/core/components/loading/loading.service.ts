import { Injectable, EventEmitter } from '@angular/core'; 
import { Subject ,  Observable } from 'rxjs';

@Injectable()
export class LoadingService {

  openDialogModal : EventEmitter<{message: string, title: string,type:string, timer?:number}> = new EventEmitter();

  
  confirmResult : EventEmitter<boolean> = new EventEmitter();

  constructor() { }

  openDialog(message : string, title: string, type:string,timer?: number) : Observable<boolean>{
    let confirmation : Subject<boolean> = new Subject;
    if (timer){
      this.openDialogModal.emit({message: message, title: title,type:type, timer:timer});
    }
    else{
      this.openDialogModal.emit({message: message, title: title,type:type});
    }
    this.confirmResult.subscribe(
      confirm => {
        confirmation.next(confirm);
        confirmation.complete();
      }
    );
    return confirmation;
  }

  openWarningDialog(message:string, title:string, timer? : number):Observable<boolean>{
    return this.openDialog(message,title,'warning',timer);
  }

  openSuccessDialog(message:string, title:string, timer? : number):Observable<boolean>{
    return this.openDialog(message,title,'success',timer);
  }

  openAskDialog(message:string, title:string, timer?: number):Observable<boolean>{
    return this.openDialog(message,title,'ask',timer);
  }

  openinformationDialog(message:string, title:string,timer?: number ){
    return this.openDialog(message,title,'information',timer);
  }

}
