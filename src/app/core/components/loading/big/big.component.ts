import { Component, OnInit } from '@angular/core';
import { LoadingService } from '../loading.service';

declare var $;

@Component({
  selector: 'app-big',
  templateUrl: './big.component.html',
  styleUrls: ['./big.component.scss']
})
export class BigComponent implements OnInit {

  message: string;
  title: string;
  type: string;
  timer: string;

  constructor(private confirmDialogService: LoadingService) { }

  ngOnInit() {
    this.confirmDialogService.openDialogModal.subscribe(
      event => {
        this.message = event.message;
        this.title = event.title;
        this.type = event.type;
        this.timer = event.timer;
        $('#confirmDialogModal').modal('show');
        if (this.timer) {
          setTimeout(() => { $('#confirmDialogModal').modal('hide') }, +this.timer)
        }
      }
    );

    $('#confirmDialogModal').on('hide.bs.modal', () => {
      this.cancel();
    })
  }

  accept() {
    this.confirmDialogService.confirmResult.emit(true);
  }

  cancel() {
    this.confirmDialogService.confirmResult.emit(false);
  }

}
