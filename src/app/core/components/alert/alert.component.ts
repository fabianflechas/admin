import { Component, OnInit } from '@angular/core';
import { AlertService } from './alert.service';
import { IAlertCommand } from './alert-command-interface';

@Component({
  selector: 'alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.css']
})
export class AlertComponent implements OnInit {

  alert: IAlertCommand;

  constructor(private alertService: AlertService) {
    this.alertService.getAlertCommand()
    .subscribe( alertCommad =>{
      this.alert = alertCommad
      if(alertCommad.lapse){
        setTimeout(() => this.closeAlert()  , this.alert.lapse);
      }
    })
  }

  closeAlert(){
    this.alert.show = false;
  }

  ngOnInit() {
  }

}
