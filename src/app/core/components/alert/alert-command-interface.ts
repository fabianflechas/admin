export interface IAlertCommand{
    class: string, 
    message: string, 
    show: boolean,
    lapse?:number,
    withLoading?: boolean 
};