import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';
import { IAlertCommand } from './alert-command-interface';



@Injectable()
export class AlertService {

  private alertComand: BehaviorSubject<IAlertCommand>= new BehaviorSubject({class:'',message:'',show:false});

  constructor() { }

  getAlertCommand(): BehaviorSubject<IAlertCommand> {
    return this.alertComand
  }
  
  showInfoAlert(message :string, withLoading:boolean, lapse? : number){
    this.alertComand.next({
      class:'alert-info',
      message:message,
      show:true,
      lapse: lapse,
      withLoading: withLoading 
    });
  }

  showSuccessAlert(message :string, withLoading:boolean, lapse? : number){
    this.alertComand.next({
      class:'alert-success',
      message:message,
      show:true,
      lapse: lapse,
      withLoading: withLoading 
    });
  }

  showErrorAlert(message :string, withLoading:boolean, lapse? : number){
    this.alertComand.next({
      class:'alert-danger',
      message:message,
      show:true,
      lapse: lapse,
      withLoading: withLoading 
    });
  }

  showWarningAlert(message :string, withLoading:boolean, lapse? : number){
    this.alertComand.next({
      class:'alert-warning',
      message:message,
      show:true,
      lapse: lapse,
      withLoading: withLoading 
    });
  }

}
