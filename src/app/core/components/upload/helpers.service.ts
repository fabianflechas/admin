import { Injectable } from '@angular/core';
import { AngularFireDatabase } from 'angularfire2/database';

import * as firebase from 'firebase/app';
import {AuthenticationService} from '../../authentication/authentication.service';


@Injectable()
export class HelpersService {

  constructor(
    private fireDatabase: AngularFireDatabase,
    private authenticationService: AuthenticationService
  ) { }

  addtoHelpers(key): Promise<firebase.database.Reference> {
    return Promise.resolve(
      this.fireDatabase.list('Helpers/')
        .push({
          uid: this.authenticationService.getCurrentUserId(),
          toothKey: key,
          timeStamp: Date.now()
        }).then(succes => {
          return this.fireDatabase.object(`Teeth/${this.authenticationService.getCurrentUserId()}/${key}`)
          .update({ help: true })
        }
        )
    );
  }
}
