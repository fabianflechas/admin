import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

interface IDataImage {
    image: HTMLImageElement;
    progress?: number;
}

@Injectable()
export class ImgLoaderService {

    constructor() { }

    loadImage(image): Observable<IDataImage> {
        return Observable
            .create(observer => {
                var data: IDataImage = {
                    image: new Image(),
                    progress: 0
                }
                data.image.src = image;
                data.image.onload = () => {
                    data.progress = 100;
                    observer.next(data);
                    observer.complete();
                };
                data.image.onerror = err => {
                    observer.error(err);
                    observer.complete();
                };
                data.image.onprogress = function(progress){ 
                    if (progress.lengthComputable) {
                        data.progress = (progress.total * progress.loaded) / 100;
                        observer.next(data)
                    }
                    console.log(data.progress);
                }

            });
    }

}
