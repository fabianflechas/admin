import { Component, OnInit, Input } from '@angular/core';
import { ImgLoaderService } from './img-loader.service';

@Component({
  selector: 'img-loading',
  templateUrl: './img-loading.component.html',
  styleUrls: ['./img-loading.component.scss']
})
export class ImgLoadingComponent implements OnInit {

  loadStatus: boolean = false;
  src: string = '';

  @Input('src') set startLoadImage(src:string){
    this.loadStatus = true;
    this.src = src;
    this.loaderService.
    loadImage(src).
    subscribe( data =>{
    })
  }

  onLoadImage(){
    this.loadStatus = false;
  }

  constructor(private loaderService: ImgLoaderService) { }

  ngOnInit() {
  }

}
