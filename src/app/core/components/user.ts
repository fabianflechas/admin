export class User {

  email : string;
  password: string;
  name : string;
  username: string;
  tutorData:{tutorName,tutorEmail};
  avatar : string;
}
