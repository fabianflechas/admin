import { Component, OnInit } from '@angular/core';
import { DialogService } from './dialog.service';

declare var $;

@Component({
  selector: 'app-dialog',
  templateUrl: './dialog.component.html',
  styleUrls: ['./dialog.component.scss']
})
export class DialogComponent implements OnInit {

  message: string;
  title: string;
  type: string;
  timer: string;

  constructor(private confirmDialogService: DialogService) { }

  ngOnInit() {
    this.confirmDialogService.openDialogModal.subscribe(
      event => {
        this.message = event.message;
        this.title = event.title;
        this.type = event.type;
        this.timer = event.timer;
        $('#confirmDialogModal').modal('show');
        if (this.timer) {
          setTimeout(() => { $('#confirmDialogModal').modal('hide') }, +this.timer)
        }
      }
    );

    $('#confirmDialogModal').on('hide.bs.modal', () => {
      this.cancel();
    })
  }

  accept() {
    this.confirmDialogService.confirmResult.emit(true);
  }

  cancel() {
    this.confirmDialogService.confirmResult.emit(false);
  }
}
