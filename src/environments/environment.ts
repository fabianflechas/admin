//  The file contents for the current environment will overwrite these during build.
//  The build system defaults to the dev environment which uses `environment.ts`, but if you do
//  `ng build --env=prod` then `environment.prod.ts` will be used instead.
//  The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  firebase: {
    apiKey: 'AIzaSyDpuarlcSKUnqZSLmPHFYyIWf-fhvUIK7g',
    authDomain: 'dentoonia-admin.firebaseapp.com',
    databaseURL: 'https://dentoonia-admin.firebaseio.com',
    projectId: 'dentoonia-admin',
    storageBucket: 'dentoonia-admin.appspot.com',
    messagingSenderId: '62940826092'
  }
};
